﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Algorithms.Tests
{
    [TestClass]
    public class EquilibriumTests
    {
        [TestMethod]
        public void SumsCorrectly()
        {
            int[] integersToBeSummed = {5, 5, 5, 5, 5};
            Assert.AreEqual(25, EquilibriumPoint.Sum(integersToBeSummed));
        }

        [TestMethod]
        public void SumsLeftCorrectly()
        {
            int[] integersToBeSummed = { 5, 5, 5, 5, 5 };
            Assert.AreEqual(10, EquilibriumPoint.SumLeft(integersToBeSummed, 2));
        }

        [TestMethod]
        public void SumsRightCorrectly()
        {
            int[] integersToBeSummed = { 5, 5, 5, 5, 5 };
            Assert.AreEqual(15, EquilibriumPoint.SumRight(integersToBeSummed, 1));
        }

        [TestMethod]
        public void FindsEquilibriumPoint_FiveIntegers()
        {
            int[] integersToBeSummed = { 5, 5, 8, 5, 5 };
            Assert.AreEqual(2, EquilibriumPoint.GetEquilibriumPoint(integersToBeSummed));
        }

        [TestMethod]
        public void FindsEquilibriumPoint_SeveralIntegers()
        {
            int[] integersToBeSummed = { 5, 7, 4, 3, 5, 9, 8, 13, 15, 25 , 1 };
            Assert.AreEqual(7, EquilibriumPoint.GetEquilibriumPoint(integersToBeSummed));
        }

        [TestMethod]
        public void ReturnsNegativeOneIfNoEPoint()
        {
            int[] integersToBeSummed = { 5, 7, 4, 3, 5, 9, 8, 13, 15, 25, 10 };
            Assert.AreEqual(-1, EquilibriumPoint.GetEquilibriumPoint(integersToBeSummed));
        }

    }
}

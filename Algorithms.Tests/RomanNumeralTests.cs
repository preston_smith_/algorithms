﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Algorithms.Tests
{
    [TestClass]
    public class RomanNumeralTests
    {
        [TestMethod]
        public void WithSingleCharacter()
        {
            Assert.AreEqual(50, RomanNumerals.Parser("L"));
        }

        [TestMethod]
        public void TestForValueWithMinorCharacter()
        {
            int x = 4;
            Assert.AreEqual(RomanNumerals.Parser("IV"), x);
        }

        [TestMethod]
        public void With3Characters()
        {
            string s = "XXX";
            Assert.AreEqual(30, RomanNumerals.Parser(s));
        }

        [TestMethod]
        public void With4Characters()
        {
            string s = "XXXV";
            Assert.AreEqual(35, RomanNumerals.Parser(s));
        }

        [TestMethod]
        public void With5Characters()
        {
            string s = "LXXXV";
            Assert.AreEqual(85, RomanNumerals.Parser(s));
        }

        [TestMethod]
        public void With3Characters_MinorCharacter()
        {
            string s = "XIX";
            Assert.AreEqual(19, RomanNumerals.Parser(s));
        }

        [TestMethod]
        public void With4Characters_MinorCharacter()
        {
            string s = "XXIX";
            Assert.AreEqual(29, RomanNumerals.Parser(s));
        }

        [TestMethod]
        public void With5Characters_MinorCharacter()
        {
            string s = "CXXIX";
            Assert.AreEqual(129, RomanNumerals.Parser(s));
        }
    }
}

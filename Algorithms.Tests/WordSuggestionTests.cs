﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Algorithms.Tests
{
    [TestClass]
    public class WordSuggestionTests
    {
        [TestMethod]
        public void AddValidWordsOnly()
        {
            Assert.Fail(WordSuggestor.AddValidWord("NOT_AWORD"));
        }
    }
}

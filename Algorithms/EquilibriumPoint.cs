﻿using System;
using System.IO;

/*

Write a function that finds the "equilibrium point" of an array of integers. The equilibrium point is the first index at which the sum of elements to the left is equal to the sum of elements to the right, not including the element at the point itself. The sum to the left of the first index and the sum to the right of the last index is considered 0. 

For example:

getEquilibriumPoint( [ 5 ] ) == 0; // left sum is 0, right sum is 0
getEquilibriumPoint( [ 0, -5 ] ) == 1; // left sum is 0, right sum is 0
getEquilibriumPoint( [ 7, -1, 13, 4, 4, -2 ] ) == 2; // left sum is 6, right sum is 6

If no equilibrium point is found, handle as you feel appropriate for the language you are using.

Your answer will be assessed on algorithmic correctness, efficiency, and clarity. Typos and other minor errors will not count against your answer.

Notes:
1) Optimize for efficiency! Imagine this running on very large arrays.
2) If possible, answer in Java, as that is the language of our existing codebase. Otherwise, choose the language you're most comfortable with.
3) Don't worry about time. A good answer is more important than a quickly completed one.
4) Keep in mind that a person is reading this. Make sure your logic is clear and be careful in usage of esoteric languages or language features.



*/

namespace Algorithms
{
    public static class EquilibriumPoint
    {
        public static int GetEquilibriumPoint(int[] arrayOfInts)
        {
            /*
            bad assumption: 
            the algorithm should sum the values on either side of the index and compare
        
            so given the following example: 
            [5, 2,7,2,7,9,1,-1,8,-4]
        
            0 =|= 2,7,2,7,9,1,-1,8,-4 (not equal) // keep going
            5 =|= 7,2,7,9,1,-1,8,-4
        
            reached the (end!) with no equality...
        
            start:
            for(x = 0; x > array.length; x++)
            {
                if(is 5 == to the sum of the remaining ints)
                    is the e point
            }
            */


            for (int x = 0; x < arrayOfInts.Length; x++)
            {
                Console.WriteLine("Current index is: " + x);
                // take the array of ints see if the sum of everything EXCEPT the first int is equal to zero
                if (x == 0)
                {
                    if ((Sum(arrayOfInts) - arrayOfInts[x]) == 0)
                    {
                        // equality point is first point
                        Console.WriteLine("Current Index {0} is the EquilibriumPoint!", x);
                        return x;
                    }
                }

                // sum up values before the current index
                // ok you need to find a way to sum up everything before the current index...

                // if current index is 3, you need 2, 1 & 0


                // sum up values after the current index

                // if equal
                if (SumLeft(arrayOfInts, x) == SumRight(arrayOfInts, x))
                {
                    // current index  is equilibrium point
                    Console.WriteLine("Current Index, '{0}' is the equilibrium point!", x);
                    return x;
                }
            }

            return -1;
        }
        public static int Sum(int[] integersToBeSummed)
        {
            var sum = 0;
            foreach (int x in integersToBeSummed)
            {
                sum += x;
            }

            return sum;
        }
        public static int SumLeft(int[] integersToBeSummed, int index)
        {
            var sum = 0;
            for (int i = index - 1; i > -1; i--)
            {
                sum += integersToBeSummed[i];
            }

            Console.WriteLine("Left sum is " + sum);

            return sum;
        }
        public static int SumRight(int[] integersToBeSummed, int index)
        {
            var sum = 0;
            for (int i = index + 1; i < integersToBeSummed.Length; i++)
            {
                sum += integersToBeSummed[i];
            }

            Console.WriteLine("Right sum is " + sum);

            return sum;
        }
    }
}
﻿using System;

namespace Algorithms
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("The value of the Roman Numeral given is {0}", RomanNumerals.Parser("IV"));
        }
    }
}
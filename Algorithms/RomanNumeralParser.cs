﻿using System;
using System.IO;
using System.Net;

namespace Algorithms
{
    public static class RomanNumerals
    {
        private static int iterations = 0;
        public static int Parser(string romanNumeral)
        {
            // break the string up into characters
            var charArray = romanNumeral.ToCharArray();
            var sum = 0;

            for (iterations = 0; iterations < romanNumeral.Length; iterations++)
            {
                // if there are at least two characters left, consider these values separately
                if (iterations != (romanNumeral.Length - 1))
                {
                    sum += CalculateValue(charArray[iterations], charArray[iterations + 1]);
                    continue;
                }

                // if the algorithm is at the last character, simply translate it's value
                sum += Translate(charArray[iterations]);
            }

            return sum;
        }
        public static int CalculateValue(char firstChar, char secondChar)
        {
            if (Translate(firstChar) < Translate(secondChar))
            {
                iterations++;
                return (Translate(secondChar) - Translate(firstChar));
            }

            return (Translate(firstChar));
        }
        public static int Translate(char charToTest)
        {
            var n = Numerals.Nothing;
            Enum.TryParse(charToTest.ToString(), out n);

            if (n != Numerals.Nothing)
                return ((int) n);

            throw new NullReferenceException("Item is not a Roman Numeral");
        }
    }

    public enum Numerals
    {
        I = 1,
        V = 5,
        X = 10,
        L = 50,
        C = 100,
        M = 1000,
        Nothing = 0
    }
}
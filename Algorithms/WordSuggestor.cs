﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Algorithms
{

/*

Write a class (or other language-appropriate data structure) that implements simple word suggestions for text entry. Given a string, the class should efficiently provide a list of all valid English words that start with that string. For the purpose of this exercise, you can assume everything will be upper case, and words will only consist of letters A to Z. 

For Java, the interface implemented by such a class might look like:
    public interface WordSuggestor {

        // Add a word to the dictionary for suggestions. Case is ignored.
        void addValidWord( String s );

        // Get the suggestions for a particular prefix. These are sorted alphabetically.
        // If there are no suggestions, returns an empty list
        // Case is ignored for finding words. Returned words will be all upper case.
        List<String> getSuggestions( String prefix );
    }

and usage would look like (if your implementation class was named MyWordSuggestor):
    WordSuggestor suggestor = new MyWordSuggestor();
    suggestor.addValidWord( "FOO" );
    suggestor.addValidWord( "BAR" );    
    suggestor.addValidWord( "BAZ" );
    suggestor.addValidWord( "FOOBAR" );
    suggestor.addValidWord( "BARBAZ" );
    suggestor.addValidWord( "FOOFOO" );
    suggestor.addValidWord( "FOOBARBAZ" );
    List<String> suggestions = suggestor.getSuggestions( "FOO" ); // returns list of "FOO", "FOOBAR", "FOOBARBAZ", "FOOFOO"
    suggestions = suggestor.getSuggestions( "FOOB" ); // returns list of "FOOBAR", "FOOBARBAZ"
    suggestions = suggestor.getSuggestions( "BING" ); // returns empty list

Your answer will be assessed on algorithmic correctness, efficiency, and clarity. Typos and other minor errors will not count against your answer.

Notes:
1) Optimize for efficiency of suggestion lookup. You don't need to worry about efficiency of word addition. You can assume that the eventual suggestion dictionary will be very large (over 1 million words) and suggestion response times need to feel near-instantaneous to users.
2) If possible, answer in Java, as that is the language of our existing codebase. Otherwise, choose the language you're most comfortable with.
3) Don't worry about time. A good answer is more important than a quickly completed one.
4) Keep in mind that a person is reading this. Make sure your logic is clear and be careful in usage of esoteric languages or language features.

*/

    public static class WordSuggestor
    {
        // These Acceptable Characters are in Alphabetic Order
        // will use this to compare characters in the Alphabetize Method at the bottom
        private const char[] AcceptableCharacters = null;
        private static List<string> DictionaryofWords { get; set; }

        public static IList<string> GetSuggestions(string word)
        {
            // make sure word is in UpperCase
            word = word.ToUpper();


            //...I think there's another thing in LINQ that I can use to quickly Alphabetize called "Sort"
            // hoping that I remember its usage correctly...

            // Get the suggestions for a particular prefix. These are sorted alphabetically.
            var suggestions = (from possibleWord in DictionaryofWords
                                where possibleWord.StartsWith(word)
                                select possibleWord).ToList();

            suggestions.Sort();

            return suggestions;

            // If there are no suggestions, returns an empty list

            // Case is ignored for finding words. Returned words will be all upper case.

        }

        public static IEnumerable<string> GetSuggestions(string word, bool useAlphabetize = true)
        {
            if (!useAlphabetize)
                return GetSuggestions(word);

            return Alphabetize((from possibleWord in DictionaryofWords
                                where possibleWord.StartsWith(word)
                                select possibleWord).ToList());
        }

        public static string AddValidWord(string word)
        {
            foreach (char c in word)
            {
                if (!(AcceptableCharacters.Contains(c)))
                {
                    throw new Exception("The word given is not valid and can't contain not Alphabetic characters");
                }
            }

            word = word.ToUpper();
            DictionaryofWords.Add(word);

            return word;
        }

        // won't need it if the Sort thing works...
        // but I'll try and write out a method anyway, just in case
        private static IEnumerable<string> Alphabetize(IList<string> groupOfWords)
        {
            var Alphabetized = new List<string>();

            while (groupOfWords.Count > 0)
            {
                // find the earliest word
                var EarliestWord = GetEarliestWord(groupOfWords);

                // add the earliest word to the new List
                Alphabetized.Add(EarliestWord);

                // pop that word from the previous stack
                groupOfWords.Remove(EarliestWord);
            }

            return Alphabetized;
        }

        private static int FindMaxCharacterLength(IEnumerable<string> groupOfWords)
        {
            var maxCharLength = 0;
            foreach (string word in groupOfWords)
            {
                if (word.Count() > maxCharLength)
                    maxCharLength = word.Count();
            }

            return maxCharLength;
        }

        private static string GetEarliestWord(IEnumerable<string> groupOfWords)
        {
            // Ok better to write out a stategy again

            // Based on an index
            // find the character at the index in each word
            // compare this character's position in the AcceptableCharacters with the previous.
            // if they are the same, move to the next character
            // return the word with the earliest character

            string currentEarliestWord = null;
            for (int x = 0; x < FindMaxCharacterLength(groupOfWords); x++)
            {
                int earliestCharacterPosition = 0;
                foreach (string word in groupOfWords)
                {
                    if (GetCharacterPosition(word, x) == earliestCharacterPosition)
                    {
                        // if they are the same position move deeper into the next character
                        currentEarliestWord = null;
                        break;
                    }
                }

                if (currentEarliestWord != null)
                    return currentEarliestWord;

                continue;
            }

            throw new Exception("Failure in the programming");
        }

        private static int GetCharacterPosition(string word, int index)
        {
            return Array.IndexOf(AcceptableCharacters, word.ToCharArray()[index]);
        }
    }
}
# Algorithms
A place to store quick experiments with algoritms and their unit tests


# What is this?
This is primarily some work I've done to play with the way that I order and automate Unit Testing.
It's also a place where I like to store quick Algorithms I do to keep my mind in shape :)
